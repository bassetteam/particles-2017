\documentclass[t]{beamer}

\usepackage[nodayofweek,level]{datetime}
\usepackage{amsmath}
\usepackage{leftidx}
\usepackage{commath}
\usepackage{mathtools}
\usepackage{amsfonts}
\usepackage{bm} % in latex required distribution (latex-tools)
\usepackage{tabu}
\usepackage{tabularx} % latex-tools bundle
\usepackage{tikz} 
\usetikzlibrary{arrows}
\usepackage{pgfplots}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subfig}
\usepackage{adjustbox}
\usetheme{Malmoe}
%\setbeamertemplate{frame numbering}[fraction]
%\usecolortheme{spruce}
\setbeamercolor{background canvas}{bg=white}
\graphicspath{{./figures/}}
\captionsetup[figure]{labelformat=empty}% redefines the caption setup of the figures environment in the beamer class.
\captionsetup[table]{labelformat=empty}
%\definecolor{mygreen}{rgb}{0.0, 0.5, 0.0}
%\usecolortheme[named=mygreen]{structure}

\title[Numerical analysis of particulate flows with the finite element method]{Numerical analysis of particulate flows with the finite element method}
%\subtitle{Doctorate Program in Structural Analysis\linebreak{}Department of Civil and Environmental Engineering}
\author[Guillermo Casas]{Guillermo Casas\\[1ex] {\small Supervisors: Eugenio O\~{n}ate, Riccardo Rossi}}
\institute{Doctorate Program in Structural Analysis\linebreak{}Department of Civil and Environmental Engineering \and Polytechnic University of Catalonia (UPC)}
\newdate{date}{30}{10}{2018}
\date{\displaydate{date}}

\begin{document}
\date{\today}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}{Overview}
\tableofcontents
\end{frame}

\section{Introduction}

\begin{frame}{Point-particle simulations}
\begin{center}
\includegraphics[width=6cm]{point-particle.pdf}
\end{center}
\end{frame}

\begin{frame}{Point-particle simulations}
\begin{enumerate}
\item Useful for particulate flows with $a \ll L$ (at least $a \ll L_h$)
\item The effect of the fluid on the particles' (forward coupling) must be modelled
\item The particles' influence on the fluid (backward coupling) must be smeared over the background mesh or ignored if $\phi_p < \sim 0.001$
\item Inter-particle effects can be taken into account (e.g., DEM) or ignored if $\phi_p < \sim 0.001$.
\item Suitable for turbulent dispersion, particle transport and erosion in ducts, simulation of sprays, etc.
\end{enumerate}
\end{frame}
\begin{frame}{The Maxey--Riley equation}
\scriptsize
\begin{equation*}
\begin{split}
m_p\frac{\mathrm{d} \bm{v}}{\mathrm{d} t} = \underbrace{  m_f \frac{\mathrm{D}\bm{u}}{\mathrm{D} t} }_{\bm{F}_U}+ 
\underbrace{ \frac{1}{2}m_f\left (\frac{\mathrm{D}}{\mathrm{D} t}  \left ( \bm{u} + \frac{1}{10}a^2 \nabla^2\bm{u} \right ) - \frac{\mathrm{d} \bm{v}}{\mathrm{d} t} \right )  }_{\bm{F}_A}
+ \underbrace{ 6\pi a \mu\left ( \bm{u} - \bm{v} + \frac{1}{6} a^2 \nabla^2\bm{u} \right ) }_{\bm{F}_D} \\
+ \underbrace{ 6a^2\sqrt{\pi\rho_f\mu}\frac{\mathrm{d} }{\mathrm{d} t}\int_{t_0 }^{t}{\frac{1}{\sqrt{t-\tau}} \left ( \bm{u} - \bm{v} + \frac{1}{6} a^2 \nabla^2\bm{u} \right ) \,\rm{d}\tau} }_{\bm{F}_H} + \underbrace{(m_f - m_p)\bm{g}}_{\bm{F}_W}\\
\end{split}
\end{equation*}
\normalsize
\begin{itemize}
\item background fluid velocity: $\bm{u}$
\item particle velocity: $\bm{v}$
\item Fax\'{e}n corrections: terms proportional to $a^2 \nabla^2\bm{u}$
\end{itemize}
\end{frame}


\begin{frame}{About the history term, $\bm{F}_H$}
\begin{block}{}
\begin{overlayarea}{\textwidth}{9cm}
\begin{itemize}
\item It can be important for $St \sim 1$ and $\rho_{\text{rel}} \sim 1$ (e.g. turbulent dispersion of organic matter, dynamics of bed saltation, etc.)
\pause
\item Its presence turns the MRE into a fractional differential equation 
\only<2>{
\begin{equation*}
\bm{F}_H = C_{H}\frac{\mathrm{d} }{\mathrm{d} t}\underbrace{\int_{t_0 }^{t}{\underbrace{\frac{1}{\sqrt{t-\tau}}}_{K_B(t-\tau)} \mathbf{w} \,\rm{d}\tau}}_{K_B * \mathbf{w}} = C_H \leftidx{^\text{RL}}{\mathrm{D}^{1/2}_{t_0^+}}{} \mathbf{w}
\end{equation*}
where $\mathbf{w} \coloneqq \mathbf{u} - \mathbf{v}$
}
\pause
\item Naive approaches lead to extremely costly simulations
\pause
\item It has been neglected almost universally in the 'applied' literature
\pause
\item For higher $Re_p$ the rate of decay of the convolution kernel is higher 
\end{itemize}
\end{overlayarea}
\end{block}
\end{frame}


\begin{frame}{Computational approaches}
\begin{block}{}
\begin{overlayarea}{\textwidth}{12cm}
Standard quadrature rules lead to extremely low accuracies (order-$1/2$). Roughly speaking, there are two strategies:
\begin{itemize}
\pause
\item Increase quadrature accuracy
\pause
\begin{itemize}
\item Use the \textbf{Gr\"{u}wald--Letnikov series} representation of the semiderivative (Bombardelli et al., 2008).
\pause 
\tiny
\only<3-5>{
\begin{equation*}
\begin{split}
^{\text{RL}}\mathrm{D}^\alpha_{t_0^+} f(t) &\coloneqq \frac{1}{\mathrm{\Gamma}(n-\alpha)}\frac{\mathrm{d}^n }{\mathrm{d} t^n}\int_{t_0}^{t} (t-\tau)^{n-\alpha-1}f(\tau) \,\mathrm{d}\tau \quad \left (\alpha = \left \lfloor \alpha \right \rfloor + 1  \right ) \\
^{\text{GL}}\mathrm{D}^\alpha_{t_0^+} f(t) &\coloneqq \lim_{h \to 0}\frac{1}{h^\alpha}\sum_{k=0}^{\left \lfloor n \right \rfloor}(-1)^k\frac{\mathrm{\Gamma}(\alpha+1)}{\mathrm{\Gamma}(k+1)\mathrm{\Gamma}(\alpha-k+1)}f(t-k h) \quad \left(n h = t-t_0 \right)
\end{split}
\end{equation*}
}
\only<5>{
\begin{equation*}
^{\text{RL}}\mathrm{D}^{1/2}_{t_0^+}  \mathbf{w}(t) = ^{\text{GL}}\mathrm{D}^{1/2}_{t_0^+} \mathbf{w}(t) \approx \frac{1}{\sqrt{h}}\sum_{k=0}^{\left \lfloor n \right \rfloor}(-1)^k\frac{\mathrm{\Gamma}(3/2)}{\mathrm{\Gamma}(k+1)\mathrm{\Gamma}(3/2-k)}\mathbf{w}(t-k h)
\end{equation*}
}
\normalsize
\only<6->{
\item Use a polynomial \textbf{interpolation/analytic approach} (Daitche, 2013)
\pause
}
\only<7>{
\scriptsize
\begin{equation*}
\int_{t_0}^{t} \frac{1}{\sqrt{t-\tau}} \mathbf{f}(\tau)\,\mathrm{d}\tau \approx 
\sqrt h \sum_{j = 0}^{n}\mu_j^n \mathbf{f}(\tau_{n-j})
\end{equation*}
\normalsize
where $\tau_n = t$ and $\tau_{0} = t+0$.
}
\end{itemize}
\pause
\only<8->{
\item Decrease memory requirements
}
\pause
\begin{itemize}
\only<9->{
\item \textbf{Conventional window method}, neglects the effect of data older than $t-t_w$. 
}
\pause
\scriptsize
\only<10>{
\begin{equation*}
\left |\mathbf{F}_H - \mathbf{F}_{H,\text{window}} \right | \leq C_H \frac{\left \| \mathbf{w} \right \|_{1,\infty}}{\sqrt{t-t_0}}
\end{equation*}
}
\normalsize
\pause
\only<11>{
\item The 2011 van Hinsberg et al. method or \textbf{method of approximation by exponentials (MAE)}.}
\item[]{}
\end{itemize}

\end{itemize}
\end{overlayarea}
\end{block}
\end{frame}

\section{The Maxey--Riley equation}
\begin{frame}{The original method of van Hinsberg et al. (2011)}
\begin{itemize}
\item It can be seen as a window method
\pause
\item It \textit{approximates}, rather than neglects, the tail contribution by approximating the kernel at the tail.
\pause
\only<3-5>{
\scriptsize
\begin{equation*}
\begin{split}
\bm{F}_H &= C_H\int_{t_0 }^{t}{K_B(t-\tau)\mathbf{f}(\tau) \,\rm{d}\tau} \\
         &\approx \underbrace{C_H\int_{t - t_w }^{t}{K_B(t-\tau) \mathbf{f}(\tau) \,\rm{d}\tau}}_{\bm{F}_{H, \text{window}}} + \underbrace{C_H\int_{t_0}^{t - t_w}{K_T(t - t_w )\mathbf{f}(\tau) \,\rm{d}\tau}}_{\bm{F}_{H, \text{tail}}}
\end{split}
\end{equation*}
\pause
\normalsize
\only<6->{The $t_i$ are points at which the corresponding exponential is tangent to $K_B$.}
}
\only<4-5>{
with
\scriptsize
\begin{equation*}
K_T(\tau) = \sum^{m}_{i=1}{a_iK_i(\tau)}
\end{equation*}
\normalsize
}
\only<5>{
\scriptsize
\begin{equation*}
K_i(\tau) = \alpha(t_i) \mathrm{e}^{\beta(t_i)\tau}\quad \alpha(t_i) = \sqrt{\frac{\mathrm{e}}{t_i}},\quad\beta(t_i) = - \frac{1}{2t_i}
\end{equation*}
\normalsize
for $i=1,\dots,m$.
}
\only<7->{
\item It uses the polynomial/analytic approach inside the window region
}
\pause
\only<8->{
\item The convolution of the exponential expansion has a recursive property that \textit{circumvents the need to store all the past history}.
\pause
\scriptsize
\only<9>{
\begin{equation*}
\bm{F}_{H, \text{tail}}(t) = C_H\sum_{i=0}^{m}{a_i \bm{F}_i(t)}
\end{equation*}
\pause
\normalsize
and it is easy to prove that
\scriptsize
\begin{equation*}
\bm{F}_i(t) = \int_{t_0 - t_w - \Delta t}^{t_0 - t_w}{ K_i(t-\tau) \bm{w} \,\textrm{d}\tau} + \mathrm{e}^{\beta(t_i) t \Delta t} \bm{F}_i(t - \Delta t)\end{equation*}
}
\normalsize
\only<10->{So \textbf{only $m$ extra vectors must be stored} (the old values of the $\bm{F}_i$), a part from the historic values of $\bm{w}$ in the window.
}
}
\pause
\only<11->{
\item The coefficients $t_i$ are fixed heuristically and $a_i$ are determined by solving an optimization problem.
}
\end{itemize}
\end{frame}

\begin{frame}{Optimization problem}
We wish to minimize the error in making the approximation $K_B \approx K_T$ in the tail region.
\pause
\begin{itemize}
\item The error itself is not available and so we look for a bound instead
\pause
\item An error bound is given by
\scriptsize
\begin{equation*}
\left |\mathbf{F}_H(t)-\mathbf{F}_{H,K}(t) \right |
\leq C_B  \frac{\left \| \mathbf{f} \right \|_{\infty }}{\sqrt{t_w}} \left (\left | 1 - \tilde{K}(1) \right | + \int_{1}^{\infty} \left | \frac{\mathrm{d} }{\mathrm{d} \tau} \left ( K_B(\tau) - \tilde{K}(\tau) \right ) \,\mathrm{d}\tau \right |  \right )
\end{equation*}
\pause
\normalsize
\item And so we look to minimize 
\scriptsize
\begin{equation*}
\left|e_{K}(1,a_{i},\tilde{t}_{i})\right|+\int_{1}^{\infty}{\left|\frac{\mathrm{\partial}}{\partial\tilde{\tau}}e_{K}(\tilde{\tau},a_{i},\tilde{t}_{i})\right|\,\mathrm{d}\tilde{\tau}}, \quad \left ( e_{K}(\tilde{\tau},a_{i},\tilde{t}_{i}) \coloneqq K_{B}(\tilde{\tau})-\tilde{K}_T(\tilde{\tau},a_{i},\tilde{t}_{i})\right )
\end{equation*}
\end{itemize}
\end{frame}

\begin{frame}{Cost functions}
\begin{equation*}
\begin{split}
I_{1}(a_{i},\tilde{t}_{i}) &\coloneqq \left|e_{K}(1,a_{i},\tilde{t}_{i})\right|+\int_{1}^{\infty}{\left|\frac{\mathrm{\partial}}{\partial\tilde{\tau}}e_{K}(\tilde{\tau},a_{i},\tilde{t}_{i})\right|\,\mathrm{d}\tilde{\tau}} \\
I_{2}(a_{i},\tilde{t}_{i}) &\coloneqq e_{K}(1,a_{i},\tilde{t}_{i})^{2}+\int_{1}^{\infty}{\left(\frac{\mathrm{\partial}}{\partial\tilde{\tau}}e_{K}(\tilde{\tau},a_{i},\tilde{t}_{i})\right)^{2}\,\mathrm{d}\tilde{\tau}} \\
I_{2t}(a_{i},\tilde{t}_{i}) &\coloneqq e_{K}(1,a_{i},\tilde{t}_{i})^{2}+\int_{1}^{\infty}{\tilde{\tau}\left(\frac{\mathrm{\partial}}{\partial\tilde{\tau}}e_{K}(\tilde{\tau},a_{i},\tilde{t}_{i})\right)^{2}\,\mathrm{d}\tilde{\tau}} \\
I_{2tH}(a_{i}) &\coloneqq I_{2t}(a_{i},\tilde{T}_{i}) = e_{K}(1,a_{i},\tilde{T}_{i})^{2}+\int_{1}^{\infty}{\tilde{\tau}\left(\frac{\mathrm{\partial}}{\partial\tilde{\tau}}e_{K}(\tilde{\tau},a_{i},\tilde{T}_{i})\right)^{2}\,\mathrm{d}\tilde{\tau}}.
\end{split}
\end{equation*}
\end{frame}

\begin{frame}{Optimal points}
\begin{center}
\begin{figure}
\includegraphics[width=7cm]{all_error_bounds.pdf}
\caption{Optimal values of $I_{1}$ obtained with different cost functions}
\end{figure}
\end{center}
\end{frame}

\section{Numerical solution of the Maxey--Riley equation}
\begin{frame}{The original method of van Hinsberg et al. (2011)}
\end{frame}
\begin{frame}{Cost functions}
\end{frame}
\begin{frame}{Optimal points}
\end{frame}

\section{Coupled FEM-DEM and applications}
\begin{frame}{First example: simple quadrature}
\end{frame}

\begin{frame}{First example: Again, calculating $\int_0^{t}{\frac{\sin(\tau)}{\sqrt{t-\tau}} \,\mathrm{d} \tau} $}
\begin{center}
\includegraphics[width=8.5cm]{comparing_norms_with_sinus.pdf}
\end{center}
\end{frame}


\begin{frame}{Second example: spiralling settling}
Analytical solution by Candelier et al. (2004)
\begin{columns}
\begin{column}{0.5\textwidth}
\begin{figure}
\begin{center}
\includegraphics[width=\textwidth]{spirals.pdf}
\end{center}
\caption{Trajectories with and without $\bm{F}_H$ at time = 100 units}
\end{figure}
\end{column}
\begin{column}{0.5\textwidth}
\begin{figure}
\begin{center}
\includegraphics[width=\textwidth]{error_decreasing_time_steps_abs_value.pdf}
\caption{Relative radial error at time = 100 units}
\end{center}
\end{figure}
\end{column}
\end{columns}
\end{frame}


\begin{frame}{Third example: dynamic system}
\begin{columns}
\begin{column}{0.5\textwidth}
\begin{itemize}
\small
\item Slowly settling particles through artificial, instationary vortices (Guseva et al. 2013)
\item Double-periodic domain, with four vortex cells
\tiny
\begin{equation*}
\begin{split}
&\mathbf{u}(t, x_1, x_2) = \begin{pmatrix}
u_1(t, x_1, x_2)\\ 
u_2(t, x_1, x_2)
\end{pmatrix} \\
&= U\left ( 1 + k\sin\left ( \omega\frac{U}{L} t \right ) \right )\begin{pmatrix}
\sin\left ( \frac{x_1}{L} \right )\cos\left ( \frac{x_2}{L} \right )\\ 
\cos\left ( \frac{x_1}{L} \right )\sin\left ( \frac{x_2}{L} \right )
\end{pmatrix}
\end{split}
\end{equation*}
\small
\item Up to 120 periods
\end{itemize}
\end{column}
\begin{column}{0.5\textwidth}
\begin{center}
\includegraphics[width=\textwidth]{Guseva.pdf}
\end{center}
\end{column}
\end{columns}
\normalsize
\end{frame}

\section{Conclusions and future work}
\begin{frame}{Modifying the algorithm}
\end{frame}

\begin{frame}{Performance}
\scriptsize
\begin{quote}
A longer interval is not possible to choose due to the numerical cost of recording the history force with small time step for this number of particles \hfill (Guseva et al., 2013)
\end{quote}
\normalsize
\begin{center}
\includegraphics[width=0.6\textwidth]{elapsed_times.pdf}
\end{center}
\end{frame}

\end{document}