\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@sectionintoc {2}{Generalizing the method of van Hinsberg et al.}{6}{0}{2}
\beamer@sectionintoc {3}{Performance}{9}{0}{3}
\beamer@sectionintoc {4}{Using quadrature substepping}{12}{0}{4}
\beamer@sectionintoc {5}{Concluding remarks}{14}{0}{5}
